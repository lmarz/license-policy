# Alpine Linux License Policy (DRAFT)
This document contains a set of guidelines, which Alpine Linux uses to determine
whether a software license is considered a free/open source software license. A
piece of software that is licensed under such licenses can be included in Alpine
Linux.

These guidelines are generally the same as the
[Open Source Definition](https://opensource.org/osd) and the [Debian Free
Software Guidelines](https://www.debian.org/social_contract#guidelines).

## Guidelines
### 1. Free Redistribution

The license shall not restrict any party from selling or giving away the
software as a component of an aggregate software distribution containing
programs from several different sources. The license shall not require a royalty
or other fee for such sale.

### 2. Source Code

The program must include source code, and must allow distribution in source code
as well as compiled form. Where some form of a product is not distributed with
source code, there must be a well-publicized means of obtaining the source code
for no more than a reasonable reproduction cost, preferably downloading via the
Internet without charge. The source code must be the preferred form in which a
programmer would modify the program. Deliberately obfuscated source code is not
allowed. Intermediate forms such as the output of a preprocessor or translator
are not allowed.

### 3. Derived Works

The license must allow modifications and derived works, and must allow them to
be distributed under the same terms as the license of the original software.

### 4. Integrity of The Author's Source Code

The license may restrict source-code from being distributed in modified form
only if the license allows the distribution of "patch files" with the source
code for the purpose of modifying the program at build time. The license must
explicitly permit distribution of software built from modified source code. The
license may require derived works to carry a different name or version number
from the original software.

### 5. No Discrimination Against Persons or Groups

The license must not discriminate against any person or group of persons.

### 6. No Discrimination Against Fields of Endeavor

The license must not restrict anyone from making use of the program in a
specific field of endeavor. For example, it may not restrict the program from
being used in a business, or from being used for genetic research.

### 7. Distribution of License

The rights attached to the program must apply to all to whom the program is
redistributed without the need for execution of an additional license by those
parties.

### 8. License Must Not Be Specific to a Product

The rights attached to the program must not depend on the program's being part
of a particular software distribution. If the program is extracted from that
distribution and used or distributed within the terms of the program's license,
all parties to whom the program is redistributed should have the same rights as
those that are granted in conjunction with the original software distribution.

### 9. License Must Not Restrict Other Software

The license must not place restrictions on other software that is distributed
along with the licensed software. For example, the license must not insist that
all other programs distributed on the same medium must be open-source software.

## Exceptions
Other types of files like documentation, firmware, data and multimedia files
should also use a license that follows these guidelines, but Alpine Linux allows
some specific exceptions for some of these files.

### Firmware
Since firmware is often required for a bootable and reasonably functioning
system, they are allowed to use a non-free license. The license must at least
allow free redistribution.  The firmware files must be standalone and
not executable by themselves (aka. binary blobs, not ELF executables).

### Documentation
For documentation the following types of licenses are also permitted:
- Creative Commons licences
- GFDL licenses

These are not compatible with the requirements above. Code must not use these
types of licenses.

## License review process
In general, licenses that have a
[SPDX License identifier](https://spdx.org/licenses/) don't need a manual review
and can be included in Alpine Linux.

A license that is neither on the list nor already in Alpine Linux has to be
manually reviewed by the license team.
The team looks at eventual prior decisions by other distributions, the Open
Source Initiative and the Free Software Foundation, though these are only seen
as hints. The team will form their own decision based on the guidelines and
common sense.
The team then publishes their decision, although it may be overruled by a higher
authority like an Alpine Linux developer.
